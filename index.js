var Web3 = require('./lib/web3');
var request = require('request');
// dont override global variable
if (typeof window !== 'undefined' && typeof window.Web3 === 'undefined') {
  window.Web3 = Web3;
}

module.exports = Web3;
var web3 = new Web3();
//var endpoint = "http://api.navicapital.com.br/api/";
var endpoint = "http://plataforma-navi.local/api/";

web3.setProvider(new web3.providers.HttpProvider());
var filter = web3.eth.filter('latest')

filter.watch(function () {
  var blocks = web3.eth.getBlock(web3.eth.blockNumber)
  var txs = blocks.transactions;

  var addresses = web3.eth.accounts;
  txs.forEach(function (currentValue, index, arr) {
    var tx = web3.eth.getTransaction(currentValue)

    addresses.forEach(function (addr) {
      if(typeof tx.to !== 'undefined'){
        if (addr === tx.to) {

          // Configure the request
          var options = {
            method: 'POST',
            headers: {
              'User-Agent': 'GETH/0.0.1',
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            form: tx
          }

          options.url = endpoint + 'notify/eth';
          options.form.value = web3.fromWei(options.form.value.toNumber(), 'ether')
          options.form.gasPrice = web3.fromWei(options.form.gasPrice.toNumber(), 'ether')

          request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
              // Print out the response body
              console.log("===================================================================")
              console.log('ENVIADA COM SUCESSO', options.form.hash)
              console.log("===================================================================")
            }else{
              console.log('ERROR', body);
            }
          })
        }
      }

    })
  })
});


// stops and uninstalls the filter
//filter.stopWatching();
